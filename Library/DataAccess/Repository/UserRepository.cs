﻿using DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public User GetByUsernameAndPassword(string username, string password)
        {
            return Items.Where(i => i.Username == username && i.Password == password).FirstOrDefault();
        }
        public bool IsUsernameFree(string username)
        {
            return Items.Any(i => i.Username == username);
        }
    }
}
