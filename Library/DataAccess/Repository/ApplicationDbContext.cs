﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataAccess.Entity;

namespace DataAccess.Repository
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<User> Users { get; set; }


        public ApplicationDbContext() : base(DbConnections.GetAppHarbourConnection())
        {
            Books = Set<Book>();
            Authors = Set<Author>();
            Genres = Set<Genre>();
            Users = Set<User>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>()
            .HasMany<Author>(g => g.Authors)
            .WithMany(f => f.Books)
            .Map(cs =>
            {
                cs.MapLeftKey("Book_Id");
                cs.MapRightKey("Author_Id");
                cs.ToTable("BookAuthors");
            });
        }
    }
}
