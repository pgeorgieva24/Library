﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entity
{
    public class Author : BaseEntity
    {
        public Author()
        {
            this.Books = new List<Book>();
        }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        public virtual List<Book> Books { get; set; }
    }
}
