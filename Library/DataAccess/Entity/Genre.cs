﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entity
{
    public class Genre : BaseEntity
    {
        public Genre()
        {
            Books = new List<Book>();
        }

        public string Name { get; set; }

        public virtual List<Book> Books { get; set; }
    }
}
