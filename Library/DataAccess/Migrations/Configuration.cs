namespace DataAccess.Migrations
{
    using DataAccess.Entity;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccess.Repository.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "DataAccess.Repository.ApplicationDbContext";
        }

        protected override void Seed(DataAccess.Repository.ApplicationDbContext context)
        {
            if (context.Users.Any()) return;
            var users = new List<User>()
                {
                    new User{ Username="admin", Password="admin", FirstName="admin", LastName="admin", IsAdmin=true },
                    new User{ Username="plamena", Password="plamena", FirstName="Plamena", LastName="Georgieva", IsAdmin=false }
                };

            users.ForEach(s => context.Users.AddOrUpdate(p => p.Username, s));
            context.SaveChanges();

            if (context.Books.Any()) return;
            var books = new List<Book>()
                {
                    new Book{ Title= "The Book Thief", ImageUrl="https://images.gr-assets.com/books/1390053681l/19063.jpg",
                        Summary =@"It�s just a small story really, about among other things: a girl, some words, an accordionist, 
                                    some fanatical Germans, a Jewish fist-fighter, and quite a lot of thievery ..." },
                    new Book{ Title = "Fantastic Beasts and Where to Find Them", ImageUrl="https://images.gr-assets.com/books/1481542648l/29363501.jpg",
                        Summary=@"When Magizoologist Newt Scamander arrives in New York, he intends his stay to be just a brief stopover. 
                                    However, when his magical case is misplaced and some of Newt's fantastic beasts escape, it spells trouble for everyone�" },
                    new Book{ Title="Harry Potter and the Sorcerer's Stone", ImageUrl = "https://images.gr-assets.com/books/1474154022l/3.jpg",
                        Summary=@"Harry Potter's life is miserable. His parents are dead and he's stuck with his heartless relatives, who force him to live 
                                    in a tiny closet under the stairs. But his fortune changes when he receives a letter that tells him the truth about himself: 
                                    he's a wizard. A mysterious visitor rescues him from his relatives and takes him to his new home, Hogwarts School of Witchcraft 
                                    and Wizardry."}
                };
            books.ForEach(s => context.Books.AddOrUpdate(p => p.Title, s));
            context.SaveChanges();

            if (context.Authors.Any()) return;
            var authors = new List<Author>()
                {
                    new Author{ Name = "Markus Zusak", ImageUrl = "https://images.gr-assets.com/authors/1376268260p5/11466.jpg",
                        Books =new List<Book>(){ books.Where(b=>b.Title == "The Book Thief").SingleOrDefault() }},
                    new Author{ Name = "J.K. Rowling", ImageUrl="https://images.gr-assets.com/authors/1510435123p5/1077326.jpg",
                        Books= new List<Book>(){ books.Where(b=>b.Title== "Fantastic Beasts and Where to Find Them").SingleOrDefault(),
                                                books.Where(b=>b.Title== "Harry Potter and the Sorcerer's Stone").SingleOrDefault()} }
                };
            authors.ForEach(s => context.Authors.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            if (context.Genres.Any()) return;
            var genres = new List<Genre>()
            {
                new Genre { Name= "Fantasy", Books=new List<Book>()
                {
                    books.Where(b => b.Title == "Fantastic Beasts and Where to Find Them").SingleOrDefault(),
                    books.Where(b=>b.Title== "Harry Potter and the Sorcerer's Stone").SingleOrDefault()
                }},
                new Genre { Name= "Fiction", Books=new List<Book>()
                {
                    books.Where(b => b.Title == "Fantastic Beasts and Where to Find Them").SingleOrDefault(),
                    books.Where(b=>b.Title== "Harry Potter and the Sorcerer's Stone").SingleOrDefault(),
                    books.Where(b=>b.Title == "The Book Thief").SingleOrDefault()
                }},
                new Genre { Name= "Historical", Books=new List<Book>() { books.Where(b=>b.Title == "The Book Thief").SingleOrDefault() }}
            };
            genres.ForEach(s => context.Genres.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

        }
    }
}

