﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.ViewModels.Authors
{
    public class AuthorViewModel
    {
        public int Id { get; set; }
        [Required]
        [RegularExpression("[A-Za-z.' ]+", ErrorMessage = "Name should contain letters only")]
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}