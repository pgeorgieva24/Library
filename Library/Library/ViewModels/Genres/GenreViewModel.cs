﻿using DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.ViewModels.Genres
{
    public class GenreViewModel
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("[A-Za-z_ ]+", ErrorMessage = "Genre should contain letters only")]
        public string Name { get; set; }
    }
}