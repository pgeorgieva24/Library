﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class UserBooksController : Controller
    {
        protected ApplicationDbContext context;
        protected BookRepository bookRepo;
        public UserBooksController()
        {
            context = new ApplicationDbContext();
            bookRepo = new BookRepository();
        }

        public ActionResult Index(int? bookId)
        {
            if (AuthenticationManager.LoggedUser == null)
                return RedirectToAction("Login", "Home");

            User user = context.Users.Where(u => u.Id == AuthenticationManager.LoggedUser.Id).FirstOrDefault();
            if (bookId == null)
                return View(user.BooksRead);

            Book book = context.Books.Where(b => b.Id == bookId.Value).FirstOrDefault();
            if (book == null)
            {
                return RedirectToAction("Index","Books");
            }

            if (book.UsersMarkedAsRead.Where(u => u.Id == AuthenticationManager.LoggedUser.Id).FirstOrDefault() == null)
            {
                user.BooksRead.Add(book);
                book.UsersMarkedAsRead.Add(user);
                book.TimesRead++;
                context.SaveChanges();
            }
            else
            {
                user.BooksRead.Remove(book);
                book.UsersMarkedAsRead.Remove(user);
                book.TimesRead--;
                context.SaveChanges();
            }

            return View(user.BooksRead);
        }
    }
}