﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using Library.ViewModels.Genres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class GenresController : Controller
    {
        private GenreRepository repo;
        public GenresController()
        {
            repo = new GenreRepository();
        }

        // GET: Genres
        public ActionResult Index()
        {
            List<Genre> genres;
            genres = repo.GetAll();
            return View(genres);
        }

        public ActionResult Create()
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                GenreViewModel model = new GenreViewModel();
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(GenreViewModel model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                Genre genre = new Genre();
                genre.Name = model.Name;
                repo.Save(genre);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                GenreViewModel model = new GenreViewModel();
                Genre genre = repo.GetById(id);
                if (genre == null)
                {
                    return RedirectToAction("Index");
                }
                model.Id = genre.Id;
                model.Name = genre.Name;

                return View(model);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(GenreViewModel model)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Genre genre = repo.GetById(model.Id);

                if (genre == null)
                {
                    return HttpNotFound();
                }

                genre.Id = model.Id;
                genre.Name = model.Name;
                repo.Save(genre);
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            if ((AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true))
            {
                Genre genre = repo.GetById(id);
                if (genre == null)
                {
                    return HttpNotFound();
                }
                repo.Delete(genre);
            }
            return RedirectToAction("Index");
        }
        
    }
}