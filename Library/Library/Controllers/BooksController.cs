﻿using DataAccess.Entity;
using DataAccess.Repository;
using Library.Models;
using Library.ViewModels.Authors;
using Library.ViewModels.Book;
using Library.ViewModels.Genres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class BooksController : Controller
    {
        protected ApplicationDbContext Context;
        private BookRepository repo;
        private GenreRepository genreRepo;
        private AuthorRepository authorRepo;
        public BooksController()
        {
            //Context = new ApplicationDbContext();
            repo = new BookRepository();
            genreRepo = new GenreRepository();
            authorRepo = new AuthorRepository();
            Context = new ApplicationDbContext();
        }


        public ActionResult Index()
        {
            List<Book> books;
            books = repo.GetAll();
            return View(books);
        }

        public ActionResult Create()
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                var authors = authorRepo.GetAll();
                var genres = genreRepo.GetAll();
                BookViewModel model = new BookViewModel();

                model.Authors = authors;
                model.Genres = genres;
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(BookViewModel model)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                if (!ModelState.IsValid||Request["author"]==null|| Request["genre"]==null)
                {
                    model.Authors = authorRepo.GetAll();
                    model.Genres = genreRepo.GetAll();
                    return View(model);
                }

                Book book = new Book();
                book.Title = model.Title;
                book.ImageUrl = model.ImageUrl;
                book.Summary = model.Summary;
                repo.Save(book);

                SetAuthorAndGenreFromCb(Request["author"], Request["genre"], model);

            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                BookViewModel model = new BookViewModel(); ;
                Book book = repo.GetById(id);

                if (book == null)
                {
                    return RedirectToAction("Index");
                }
                model.Id = book.Id;
                model.Title = book.Title;
                model.ImageUrl = book.ImageUrl;
                model.Summary = book.Summary;                
                model.Authors = book.Authors;
                foreach (var item in book.Authors)
                {
                    model.SelectedAuthor.Add(item.Id);
                }
                foreach (var item in book.Genres)
                {
                    model.SelectedGenre.Add(item.Id);
                }
                model.Authors = authorRepo.GetAll();
                model.Genres = genreRepo.GetAll();

                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(BookViewModel model)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                if (!ModelState.IsValid || Request["author"] == null || Request["genre"] == null)
                {
                    model.Authors = authorRepo.GetAll();
                    model.Genres = genreRepo.GetAll();
                    return View(model);
                }

                Book book = Context.Books.Where(b => b.Id == model.Id).FirstOrDefault();

                if (book == null)
                {
                    return HttpNotFound();
                }

                book.Id = model.Id;
                book.Title = model.Title;
                book.ImageUrl = model.ImageUrl;
                book.Summary = model.Summary;
                GetCheckedIds(model.SelectedAuthor, Request["author"].ToList());
                GetCheckedIds(model.SelectedGenre, Request["genre"].ToList());


                List<int> oldAuthorsIds = new List<int>();
                foreach (var author in book.Authors)
                {
                    oldAuthorsIds.Add(author.Id);
                }
                List<int> oldGenresIds = new List<int>();
                foreach (var genre in book.Genres)
                {
                    oldGenresIds.Add(genre.Id);
                }

                SetAuthorsFromCb(model.SelectedAuthor, book, oldAuthorsIds);
                SetGenresFromCb(model.SelectedGenre, book, oldGenresIds);

                Context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (AuthenticationManager.LoggedUser != null && AuthenticationManager.IsAdmin() == true)
            {
                Book book = repo.GetById(id);
                if (book == null)
                {
                    return HttpNotFound();
                }
                repo.Delete(book);
            }
            return RedirectToAction("Index");
        }

        private void SetAuthorsFromCb(List<int> items, Book book, List<int> oldAuthorsIds)
        {
            if (items.Count != book.Authors.Count || !items.SequenceEqual(oldAuthorsIds))
            {
                List<int> duplicatingAuthors = new List<int>();
                AreDifferentItemsFromCb(items, oldAuthorsIds, duplicatingAuthors);
                if (oldAuthorsIds.Count > 0)
                {
                    RemoveAuthorFromBook(book, oldAuthorsIds);
                }
                if (items.Count > 0)
                {
                    AddAuthorToBook(items, book);
                }
            }
        }

        private void SetGenresFromCb(List<int> items, Book book, List<int> oldGenresIds)
        {
            if (items.Count != book.Genres.Count || !items.SequenceEqual(oldGenresIds))
            {
                List<int> duplicatingAuthors = new List<int>();
                AreDifferentItemsFromCb(items, oldGenresIds, duplicatingAuthors);
                if (oldGenresIds.Count > 0)
                {
                    RemoveGenreFromBook(book, oldGenresIds);
                }
                if (items.Count > 0)
                {
                    AddGenresToBook(items, book);
                }
            }
        }

        private void RemoveAuthorFromBook(Book book, List<int> oldAuthorsIds)
        {
            foreach (var item in oldAuthorsIds)
            {
                Author author = Context.Authors.Where(g => g.Id == item).FirstOrDefault();
                book.Authors.Remove(author);
                Context.SaveChanges();
            }
        }
        private void RemoveGenreFromBook(Book book, List<int> oldGenresIds)
        {
            foreach (var item in oldGenresIds)
            {
                Genre genre = Context.Genres.Where(g => g.Id == item).FirstOrDefault();
                book.Genres.Remove(genre);
                Context.SaveChanges();
            }
        }

        private static void AreDifferentItemsFromCb(List<int> items, List<int> oldSetOfIds, List<int> duplicatingIds)
        {
            foreach (var item in items)
            {
                if (oldSetOfIds.Contains(item))
                {
                    duplicatingIds.Add(item);
                }
            }
            foreach (var item in duplicatingIds)
            {
                oldSetOfIds.Remove(item);
                items.Remove(item);
            }
        }

        private void AddAuthorToBook(List<int> items, Book book)
        {
            foreach (var item in items)
            {
                Author author = Context.Authors.Where(g => g.Id == item).FirstOrDefault();
                book.Authors.Add(author);
                Context.SaveChanges();
            }
        }

        private void AddGenresToBook(List<int> cbValue, Book book)
        {
            foreach (int id in cbValue)
            {
                Genre genre = Context.Genres.Where(a => a.Id == id).FirstOrDefault();
                book.Genres.Add(genre);
                Context.SaveChanges();
            }
        }



        private void SetAuthorAndGenreFromCb(string authorCb, string genreCb, BookViewModel model)
        {
            List<int> cbValue = new List<int>();
            List<char> checkbox = authorCb.ToList();
            GetCheckedIds(cbValue, checkbox);
            Book book = Context.Books.Where(b => b.Title == model.Title).FirstOrDefault();

            AddAuthorToBook(cbValue, book);
            checkbox.Clear();
            cbValue.Clear();
            checkbox = genreCb.ToList();

            GetCheckedIds(cbValue, checkbox);
            AddGenresToBook(cbValue, book);

        }

        private void GetCheckedIds(List<int> cbValue, List<char> checkbox)
        {
            foreach (var c in checkbox)
            {
                if (int.TryParse(c.ToString(), out int id))
                {
                    cbValue.Add(id);
                }
            }
        }
    }
}